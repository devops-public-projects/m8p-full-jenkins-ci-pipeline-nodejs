#!/user/bin/env groovy

def call() {
    echo "Committing Version Increment"

    sh 'git config --global user.email "jenkins@example.com"'
    sh 'git config --global user.name "jenkins"'

    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devops-public-projects/m8p-jenkins-exercises.git"
        sh 'git add .'
        sh 'git commit -m "Jenkins: Version Bump"'
        sh 'git push origin HEAD:main'
    }
}