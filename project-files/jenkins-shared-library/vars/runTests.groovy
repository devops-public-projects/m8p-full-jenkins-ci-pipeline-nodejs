#!/user/bin/env groovy

def call() {
    echo "Running NPM Tests"

    sh 'npm --prefix ./app install'

    try {
        sh 'npm --prefix ./app run test'
        echo "Successful Tests"
    }
    catch (e) {
        echo "Failed Tests"
        echo "${e.toString()}"
        sh 'exit 1'
    }
}