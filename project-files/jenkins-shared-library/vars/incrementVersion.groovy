#!/user/bin/env groovy

def call() {
    echo "Incrementing the App Version"
    sh 'npm --prefix ./app version patch'

    def packageJsonText = sh(script: 'cat ./app/package.json', returnStdout: true).trim()
    def packageJson = readJSON text: packageJsonText

    env.IMAGE_NAME = "theinstinct/demo-app:${packageJson.version}-${BUILD_NUMBER}"
}