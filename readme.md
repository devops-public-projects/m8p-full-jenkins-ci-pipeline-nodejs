
# Build Automation & CI/CD with Jenkins
This project will simulate an entire build automation in Jenkins for a NodeJS App.

## Technologies Used
- Jenkins
- Docker
- GitLab
- Git
- JavaScript
- NodeJS
- NPM


## Project Description
- Dockerize a NodeJS App
- Create a Full Pipeline in Jenkins
	- Dynamically Incrementing Version
	- Run Tests
	- Build a Docker Image
	- Push to Private Docker Repository
	- Commit to Git
- Manually Deploy a Docker Image to a Droplet Server
- Extract Logic into a Jenkins Shared Library

## Prerequisites
- Jenkins installed on a remote server as a Docker container
- Docker Hub account
- Gitlab account and a repository
- DigitalOcean account to create Droplets
- Private Repository with the **java-maven-app** code in it. [Gitlab Link](https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/jenkins-exercises)

## Guide Steps
### Exercise 1 | Dockerize NodeJS App
#### Clone Code Repo
- `git clone https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/jenkins-exercises.git`
- Upload the code into a new GitLab repository that will be used with Jenkins. I will call my repo **m8p-jenkins-exercises**.
	- Perform a `rm -rf .git` to remove the old repositories information

#### Create a Jenkinsfile
- The main stages will consist of
	- Incrementing the build version using `npm version patch`
	- Packaging the application with `npm pack app/`
	- Running tests with `npm test FILE.tgz`
	- Create a Docker image and Upload to our Docker Repo
	- Commit our version increment changes to GitLab

#### Create a Dockerfile
- The main stages of the Dockerfile are
	- Import an Alpine Linux base image
	- Create a directory for our application
	- Copy the packaged app to the image
	- Run  `node server.js` to start the app in the container

### Exercise 2 | Create a Full Pipeline for a NodeJS App
This exercise will utilize the previous Jenkinsfile and Dockerfile above to automate the entire pipeline. We will configure out pipeline in Jenkins to pull from our repository and allow GitLab to notify Jenkins when a change is made to trigger a new build.

#### Configure Jenkins Pipeline
- Dashboard > New Item
	- Name: **my-pipeline**
	- Type: **Multibranch**
	- **OK**
- Dashboard > my-pipeline > **Configure**
	- Branch Sources (Git)
		- Project Repository: **REPOSITORY_URL.git**
		- Credentials: **REPOSITORY_CREDENTIALS**
		- Behaviors
			- Filter by name (with Regular Expression): **.***
		- Property Strategy: All branches get the same properties
	- Build strategies: Ignore Committer Strategy
		- Don't trigger builds for pushes.. : jenkins@example.com
		- Check: **Allow builds when a changeset contains non-ignored author(s)**
	- Build Configuration
		- Scan by webhook
			- Trigger token: **m8pmultibranch**
	- **Save**

#### Configure GitLab Webhook
- From your GitLab project go to Settings > **Webhooks**
- **Add new webhook**
	- URL: `http://JENKINS_SERVER_IP:JENKINS_PORT/multibranch-webhook-trigger/invoke?token=m8pmultibranch`
	- Trigger: **Push events + all branches**
	- **Save Changes**

When a build gets pushed we should now see it appear in our private Docker Hub Repository.
![Successful Docker Upload](/images/m8p-successful-docker-upload.png)


### Exercise 3 | Manually Deploy a Docker Image to a Server
- On your droplet server that has Docker installed 
- `docker login`
- `docker pull YOUR_REPO/demo-app:VERSION_NUMBER`
- `docker run -d -p 3000:3000 IMAGE_ID`
	- Make sure to open port 3000 on DigitalOcean if you are using a firewall for your droplets!

![Successful Docker Run](/images/m8p-successful-docker-run.png)

![Successful Application Running](/images/m8p-application-running-successfully.png)


### Exercise 4 | Extract Into Jenkins Shared Library
#### Configure Global Jenkins Shared Library
- Create a Repository for your shared library, I have one configured called `jenkins-shared-library`. It includes the basic files for `Jenkinsfile` and `Docker` functions
- Dashboard > Manage Jenkins > System > Global Pipeline Libraries > **Add**
	- Name: **jenkins-shared-library**
	- Default Version: **master** or **main**
	- Retrieval Method: **Modern SCM**
		- SCM: **Git**
		- Project Repository: **SHARED_LIBRARY_REPOSITORY_URL.git**
		- Credentials: **GITLAB_CREDENTIALS_ID**
	- **Save**

Inside the repository, I have several files for the functions inside the previous `Jenkinsfile`.
- `src/com/example/Docker.groovy` contains the Docker logic
- `vars` Holds all non-Docker functions that may reference `Docker.groovy`


#### Modify Jenkinsfile
- All logic was moved into one of the shared library groovy functions
	- `incrementVersion`
	- `runTests`
	- `packageApp`
	- `buildImage` > `Docker.groovy`
	- `dockerLogin` > `Docker.groovy`
	- `dockerPush` > `Docker.groovy`
	- `commitVersion`

I have a successful build for version 1.0.2-4 meaning all of the logic from Jenkinsfile to the groovy scripts was successful!

![Successfully Moved Logic to a Shared Librarry](/images/m8p-successful-shared-library.png)
